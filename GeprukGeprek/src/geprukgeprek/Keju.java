/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package geprukgeprek;


public class Keju extends LaukDecorator {
    Makanan makanan;
    
    public Keju(Makanan makanan){
        this.makanan = makanan;
    }
    
    /**
     *
     * @return
     */
    @Override
    public String getDescription(){
        return makanan.getDescription() + ", Keju";
    }
    
    /**
     *
     * @return
     */
    @Override
    public double cost(){
        return 6000 + makanan.cost();
    }
}
