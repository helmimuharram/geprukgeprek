/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package geprukgeprek;


public class Teh extends MinumanDecorator{
    Makanan makanan;
    
    public Teh(Makanan makanan){
        this.makanan = makanan;
    }
    
    @Override
    public String getDescription(){
        return makanan.getDescription() + ", Teh";
    }
    
    /**
     *
     * @return
     */
    @Override
    public double cost(){
        return 5000 + makanan.cost();
    }
}
