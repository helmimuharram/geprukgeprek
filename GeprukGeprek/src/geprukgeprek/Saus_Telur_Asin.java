/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package geprukgeprek;


public class Saus_Telur_Asin extends LaukDecorator{
    Makanan makanan;
    
    public Saus_Telur_Asin(Makanan makanan){
        this.makanan = makanan;
    }
    
    @Override
    public String getDescription(){
        return makanan.getDescription() + ", Saus_Telur_Asin";
    }
    
    /**
     *
     * @return
     */
    @Override
    public double cost(){
        return 6000 + makanan.cost();
    }
}
