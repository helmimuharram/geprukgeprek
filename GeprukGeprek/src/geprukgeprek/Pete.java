/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package geprukgeprek;


public class Pete extends LaukDecorator{
    Makanan makanan;
    
    public Pete(Makanan makanan){
        this.makanan = makanan;
    }
    
    @Override
    public String getDescription(){
        return makanan.getDescription() + ", Pete";
    }
    
    /**
     *
     * @return
     */
    @Override
    public double cost(){
        return 6000 + makanan.cost();
    }
}
