/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package geprukgeprek;


public class Dadar extends LaukDecorator {
    Makanan makanan;
    
    public Dadar(Makanan makanan){
        this.makanan = makanan;
    }
    
    /**
     *
     * @return
     */
    @Override
    public String getDescription(){
        return makanan.getDescription() + ", Dadar";
    }
    
    /**
     *
     * @return
     */
    @Override
    public double cost(){
        return 2000 + makanan.cost();
    }
}
