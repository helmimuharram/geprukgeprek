/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package geprukgeprek;


public class Jeruk extends MinumanDecorator{
    Makanan makanan;
    
    public Jeruk(Makanan makanan){
        this.makanan = makanan;
    }
    
    /**
     *
     * @return
     */
    @Override
    public String getDescription(){
        return makanan.getDescription() + ", Jeruk";
    }
    
    /**
     *
     * @return
     */
    @Override
    public double cost(){
        return 7000 + makanan.cost();
    }
}
