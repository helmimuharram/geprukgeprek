/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package geprukgeprek;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author ASUS
 */
public class MakananTest {
    
    public MakananTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of getDescription method, of class Makanan.
     */
    @Test
    public void testGetDescription() {
        System.out.println("getDescription");
        Makanan instance = new MakananImpl();
        String expResult = "";
        String result = instance.getDescription();
        assertEquals(expResult, result);
    }

    /**
     * Test of cost method, of class Makanan.
     */
    @Test
    public void testCost() {
        System.out.println("cost");
        Makanan instance = new MakananImpl();
        double expResult = 0.0;
        double result = instance.cost();
        assertEquals(expResult, result, 0.0);
    }

    public class MakananImpl extends Makanan {

        /**
         *
         * @return
         */
        @Override
        public double cost() {
            return 0.0;
        }
    }
    
}
